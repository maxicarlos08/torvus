# Torvus

Torvus is a Discord bot designed to act as a bridge between veloren servers and discord servers

## `.env.example`

You should rename this file to `.env` and fill it out with the appropriate bot configuration.

## Running

Execute `cargo run` to run the bot.

# Development

In case you want to build against a server in the weekly or nightly tag (e.g. official server), you need to use the bind.sh script to switch to the respective tag of the server
