#!/bin/bash

CURRENT=$(grep "https://gitlab.com/veloren/veloren" Cargo.toml | head -n1 | pcregrep -o1 'branch = "(.*?)"')
IS_BRANCH=true
NEW=$1

if [ "$CURRENT" = "" ]; then
  CURRENT=$(grep "https://gitlab.com/veloren/veloren" Cargo.toml | head -n1 | pcregrep -o1 'tag = "(.*?)"')  
  IS_BRANCH=false
fi

if [ "$1" = "" ]; then
  echo "Usage: $0 <branch to be switched to>"
  echo "torvus by default builds against veloren master"
  echo "in case you want to build against weekly or nightly tag, e.g. in order to connect to official server, use this script to adjust Cargo.toml"
  if [ "$IS_BRANCH" = true ]; then
    echo "Current branch is set to: ${CURRENT}"
  else
    echo "Current tag is set to: ${CURRENT}"
  fi
  exit
fi

if [ "$IS_BRANCH" = true ]; then
  echo "Current branch is set to: ${CURRENT}"
  sed -i "s/, branch = \"${CURRENT}\"/, tag = \"${NEW}\"/" Cargo.toml
else
  echo "Current tag is set to: ${CURRENT}"
  sed -i "s/, tag = \"${CURRENT}\"/, tag = \"${NEW}\"/" Cargo.toml
fi

NEW=$(grep "https://gitlab.com/veloren/veloren" Cargo.toml | head -n1 | pcregrep -o1 'tag = "(.*?)"')
echo "switched to tag: ${NEW}"
