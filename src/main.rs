use metrics::TorvusMetrics;
use version::{GIT_DATE, GIT_HASH, GIT_TAG};

use dotenv::dotenv;
use prometheus::Registry;
use prometheus_hyper::Server as PrometheusServer;
use serenity::{
    async_trait,
    model::{
        channel::Message,
        event::ResumedEvent,
        gateway::{Activity, Ready},
        id::{ChannelId, GuildId},
        user::OnlineStatus,
    },
    prelude::*,
    utils::Colour,
};
use std::{env, net::SocketAddr, sync::Arc, thread, time::Duration};
use tokio::runtime::Runtime;
use tokio::sync::Notify;
use tracing::{debug, error, info};
use tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, EnvFilter};
use veloren_client::{
    addr::ConnectionArgs, Client as VelorenClient, Event as VelorenEvent, ServerInfo,
};
use veloren_client_i18n::LocalizationHandle;
use veloren_common::{
    clock::Clock, comp, util::DISPLAY_VERSION_LONG, util::GIT_DATE as VELOREN_GIT_DATE,
    util::GIT_HASH as VELOREN_GIT_HASH,
};
use veloren_voxygen_i18n_helpers::localize_chat_message;

mod metrics;
mod version;

const TPS: u64 = 60;

enum VelorenMessage {
    Connect,
    Disconnect,
    Chat(String, Colour),
    Players(String, String),
    Version(ServerInfo),
    Help,
}

enum DiscordMessage {
    Chat { nickname: String, msg: Message },
}

struct Handler {
    messages_rx: async_channel::Receiver<VelorenMessage>,
    server_addr: ConnectionArgs,
    guild: u64,
    bridge_channel: u64,
    discord_msgs_tx: async_channel::Sender<DiscordMessage>,
    torvus_metrics: Arc<TorvusMetrics>,
    _metrics_shutdown: Arc<Notify>,
}

async fn connect_to_veloren(
    addr: ConnectionArgs,
    torvus_metrics_clone: &Arc<TorvusMetrics>,
    messages_tx: &async_channel::Sender<VelorenMessage>,
    veloren_username: &str,
    veloren_password: &str,
    trusted_auth_server: &str,
    runtime: Arc<Runtime>,
) -> veloren_client::Client {
    let mut retry_cnt = 0u32;
    'connect: loop {
        debug!("Connecting...");
        let mut mismatched_server_info = None;
        let veloren_client = match VelorenClient::new(
            addr.clone(),
            Arc::clone(&runtime),
            &mut mismatched_server_info,
            veloren_username,
            veloren_password,
            None,
            |auth_server| auth_server == trusted_auth_server,
            &|_| {},
            |_| {},
        )
        .await
        {
            Ok(client) => client,
            Err(e) => {
                error!(
                    "Failed to connect to Veloren server: {:?}, retry: {}",
                    e, retry_cnt
                );
                if let Some(server_info) = mismatched_server_info {
                    error!(
                        "This is likely due to a version mismatch: Client Version {}-{}, Server Version {}-{}",
                        *VELOREN_GIT_HASH, *VELOREN_GIT_DATE,
                        server_info.git_hash, server_info.git_date
                    )
                }
                retry_cnt += 1;
                torvus_metrics_clone.veloren_connected_error.inc();
                tokio::time::sleep(Duration::from_millis(500) * retry_cnt).await;
                continue 'connect;
            }
        };

        messages_tx.try_send(VelorenMessage::Connect).unwrap();

        debug!("Logged in.");

        return veloren_client;
    }
}

#[allow(clippy::too_many_arguments)]
impl Handler {
    pub fn new(
        addr: ConnectionArgs,
        guild: u64,
        bridge_channel: u64,
        veloren_username: String,
        veloren_password: String,
        trusted_auth_server: String,
        torvus_metrics: Arc<TorvusMetrics>,
        metrics_shutdown: Arc<Notify>,
        runtime: Arc<Runtime>,
    ) -> Self {
        let (messages_tx, messages_rx) = async_channel::bounded(100);
        let (discord_msgs_tx, discord_msgs_rx) = async_channel::bounded(100);

        let torvus_metrics_clone = Arc::clone(&torvus_metrics);
        let addr_clone = addr.clone();
        let localisation = LocalizationHandle::load_expect("en");
        tokio::task::spawn_blocking(move || {
            let mut retry_cnt = 0u32;

            'connect: loop {
                let mut veloren_client = runtime.block_on(connect_to_veloren(
                    addr.clone(),
                    &torvus_metrics_clone,
                    &messages_tx,
                    &veloren_username,
                    &veloren_password,
                    &trusted_auth_server,
                    Arc::clone(&runtime),
                ));

                let mut clock = Clock::new(Duration::from_secs_f64(1.0 / TPS as f64));

                loop {
                    let events =
                        match veloren_client.tick(comp::ControllerInputs::default(), clock.dt()) {
                            Ok(events) => events,
                            Err(e) => {
                                error!("Failed to tick client: {:?}, retry: {}", e, retry_cnt);
                                retry_cnt += 1;
                                thread::sleep(Duration::from_secs(10) * retry_cnt);
                                messages_tx.try_send(VelorenMessage::Disconnect).unwrap();
                                continue 'connect;
                            }
                        };
                    retry_cnt = 0;

                    if let Ok(DiscordMessage::Chat { nickname, msg }) = discord_msgs_rx.try_recv() {
                        let msg: Message = msg;
                        let message = deunicode::deunicode(&msg.content).to_string();
                        if !message.starts_with('/') {
                            veloren_client.send_chat(format!("{}: {}", nickname, message));
                        } else {
                            match message.as_str() {
                                "/players" => {
                                    veloren_client.send_command("players".to_string(), vec![])
                                }
                                "/version" => messages_tx
                                    .try_send(VelorenMessage::Version(
                                        veloren_client.server_info().clone(),
                                    ))
                                    .unwrap(),
                                m => {
                                    if m != "/help" {
                                        info!("Intercepted unknown command: {}", message)
                                    }
                                    messages_tx.try_send(VelorenMessage::Help).unwrap()
                                }
                            }
                        }
                        torvus_metrics_clone.messages_to_veloren.inc();
                    }

                    for event in events {
                        match event {
                            VelorenEvent::Chat(msg) => {
                                // Skip over our own messages
                                if veloren_client
                                    .uid()
                                    .is_some_and(|client_uid| msg.uid() == Some(client_uid))
                                {
                                    continue;
                                };

                                torvus_metrics_clone.messages_from_veloren.inc();
                                let message = localize_chat_message(
                                    msg.clone(),
                                    |msg| veloren_client.lookup_msg_context(msg),
                                    &localisation.read(),
                                    true,
                                )
                                .1
                                // Remove all escape characters before further sanitizing
                                .replace('\\', "")
                                // Escape all the formatting characters
                                .replace('*', r"\*")
                                .replace('_', r"\_")
                                .replace('`', r"\`")
                                .replace('~', r"\~")
                                // Use zero-width spaces to prevent sending formatted URLs and mentions
                                .replace('(', "\u{200B}(") // [Formatted](URLs)
                                .replace('<', "<\u{200B}") // #mentions, @mentions, :emojis:
                                .replace('@', "@\u{200B}"); // @everyone, @here

                                if message.contains(" online players:") {
                                    if let Some(index) = message.find('\n') {
                                        let (title, players) = message.split_at(index);
                                        messages_tx
                                            .try_send(VelorenMessage::Players(
                                                title.to_string(),
                                                players
                                                    .to_string()
                                                    .replace('[', "**[")
                                                    .replace(']', "]** "),
                                            ))
                                            .unwrap();
                                    }
                                } else {
                                    use veloren_common::comp::chat::ChatType;
                                    let colour = match msg.chat_type {
                                        ChatType::Online(_) => Colour::TEAL,
                                        ChatType::Offline(_) => Colour::PURPLE,
                                        ChatType::Kill(_, _) => Colour::RED,
                                        ChatType::World(_) => Colour::BLURPLE,
                                        _ => Colour::GOLD,
                                    };

                                    let message = match msg.chat_type {
                                        ChatType::Online(_) | ChatType::Offline(_) => message
                                            .find(']')
                                            .map(|index| {
                                                // It's safe to increment index by 1 here because the size of the ']' character is always a single UTF-8 codepoint.
                                                let (user, message) = message.split_at(index + 1);
                                                format!("***{}*** *{}*", user, message.trim())
                                            })
                                            .unwrap_or(message),
                                        ChatType::World(uid) => message
                                            .find(':')
                                            .map(|index| {
                                                let char_name = veloren_client
                                                    .player_list()
                                                    .get(&uid)
                                                    .map(|player_info| {
                                                        player_info.character.as_ref().map(
                                                            |char_info| {
                                                                char_info.name.trim().to_owned()
                                                            },
                                                        )
                                                    });

                                                // The URL contains the player's character name instead of just a placeholder.
                                                // This isn't super useful, but I think it's neat c:
                                                let url_prefix = match char_name {
                                                    Some(Some(name)) => {
                                                        name
                                                            // Ending with a dot here would make the URL invalid, which breaks the formatting
                                                            .trim_matches('.')
                                                            // This is so you don't get %XX codes in the URL which make the name less readable
                                                            .chars()
                                                            .map(|c| match c {
                                                                c if c.is_whitespace() => '_',
                                                                c if !c.is_ascii() => '+',
                                                                c => c,
                                                            })
                                                            .collect::<String>()
                                                            // This is to make it more obvious what the link is about
                                                            + ".is_their_character_name"
                                                    }
                                                    Some(None) => "has_no_character".to_owned(),
                                                    None => "player_does_not_exist".to_owned(),
                                                };

                                                let (user, message) = message.split_at(index);
                                                format!(
                                                    // URLs ending in .invalid are guaranteed to not be registered
                                                    // according to IETF's RFC 2606 https://tools.ietf.org/html/rfc2606
                                                    "**[{}](https://{}.invalid){}**",
                                                    // `\_` is taken literally by discord here, unescape it
                                                    user.replace(r"\_", "_"),
                                                    url_prefix,
                                                    message.trim_end()
                                                )
                                            })
                                            .unwrap_or(message),
                                        ChatType::Kill(_, _) => format!(
                                            "*{}*",
                                            message
                                                .replace('[', "**[")
                                                .replace(']', "]**")
                                                // Discord's Markdown parser is less forgiving on Android
                                                .trim()
                                                .replace("]** ", "]*** *")
                                                .replace(" **[", "* ***[")
                                        ),
                                        _ => format!("*{}*", message.trim()),
                                    };

                                    messages_tx
                                        .try_send(VelorenMessage::Chat(message, colour))
                                        .unwrap();
                                }
                            }
                            VelorenEvent::Disconnect => {
                                torvus_metrics_clone.messages_from_veloren.inc();
                                messages_tx.try_send(VelorenMessage::Disconnect).unwrap();
                            }
                            VelorenEvent::DisconnectionNotification(_) => {
                                debug!("Will be disconnected soon! :/")
                            }
                            VelorenEvent::Notification(notification) => {
                                debug!("Notification: {:?}", notification);
                            }
                            _ => {}
                        }
                    }
                    veloren_client.cleanup();

                    clock.tick();
                }
            }
        });

        Self {
            messages_rx,
            server_addr: addr_clone,
            guild,
            bridge_channel,
            discord_msgs_tx,
            torvus_metrics,
            _metrics_shutdown: metrics_shutdown,
        }
    }
}

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        info!("Connected as {}", ready.user.name);

        ctx.set_presence(Some(Activity::playing("Veloren")), OnlineStatus::Online)
            .await;

        let channel_id = ChannelId(self.bridge_channel);
        let server_addr = self.server_addr.clone();
        let messages_rx = self.messages_rx.clone();

        let messages_to_discord_clone = self.torvus_metrics.messages_to_discord.clone();

        let _handle = tokio::spawn(async move {
            while let Ok(msg) = messages_rx.recv().await {
                let send_msg = |text, colour| {
                    channel_id.send_message(&ctx.http, move |m| {
                        m.embed(|e| e.colour(colour).description(text))
                    })
                };

                let send_players_list = |(title, players)| {
                    channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e| e.colour(Colour::ORANGE).title(title).description(players))
                    })
                };

                let send_version_info = |server_info: ServerInfo| {
                    channel_id.send_message(&ctx.http, move |m| {
                        m.embed(|e| {
                            e.colour(Colour::ORANGE)
                                .title("Version Information")
                                .field(
                                    "Torvus",
                                    format!("{}-{} ({})", GIT_TAG, *GIT_HASH, *GIT_DATE),
                                    false,
                                )
                                .field(
                                    "Veloren Client",
                                    format!("{} ({})", *VELOREN_GIT_HASH, *VELOREN_GIT_DATE),
                                    true,
                                )
                                .field(
                                    "Veloren Server",
                                    format!("{} ({})", server_info.git_hash, server_info.git_date),
                                    true,
                                )
                        })
                    })
                };

                let send_help = || {
                    channel_id.send_message(&ctx.http, |m| {
                        m.embed(|e|
                            e.colour(Colour::ORANGE).
                            title("Help")
                            .description("Torvus lets Discord users talk to Veloren players. \n\n**There are a few commands available for you to use:**")
                            .field("/players", "Shows who is currently playing on the Veloren server.", false)
                            .field("/version", "Shows the versions of Torvus, it's internal Veloren Client, and the Veloren Server it's connected to.", false)
                            .field("/help", "Shows this message.", false)
                        )
                    })
                };

                match msg {
                    VelorenMessage::Chat(chat_msg, colour) => {
                        if !chat_msg.starts_with("**[[You]") {
                            debug!("[Veloren] {}", chat_msg);
                            if let Err(e) = send_msg(chat_msg, colour).await {
                                error!("Failed to send discord message: {}", e);
                            } else {
                                messages_to_discord_clone.inc();
                            }
                        }
                    }
                    VelorenMessage::Players(title, players) => {
                        debug!("[Veloren] {} {}", title, players);
                        if let Err(e) = send_players_list((title, players)).await {
                            error!("Failed to send discord message: {}", e);
                        } else {
                            messages_to_discord_clone.inc();
                        }
                    }
                    VelorenMessage::Connect => {
                        ctx.online().await;
                        info!("Server bridge connected to {:?}.", server_addr);
                    }
                    VelorenMessage::Disconnect => {
                        ctx.invisible().await;
                        info!("Server bridge disconnected from {:?}.", server_addr);
                    }
                    VelorenMessage::Version(server_info) => {
                        if let Err(e) = send_version_info(server_info).await {
                            error!("Failed to send discord message: {}", e)
                        }
                    }
                    VelorenMessage::Help => {
                        if let Err(e) = send_help().await {
                            error!("Failed to send discord message: {}", e)
                        }
                    }
                }
            }
        });
    }

    async fn resume(&self, _: Context, _: ResumedEvent) {
        info!("Connection to discord resumed.");
    }

    async fn message(&self, ctx: Context, msg: Message) {
        let guild = GuildId(self.guild);

        if *msg.channel_id.as_u64() != self.bridge_channel {
            return;
        }

        // Ignore bots, accomodate webhook-based X -> Discord bridges
        if !msg.author.bot || msg.webhook_id.is_some() {
            self.torvus_metrics.messages_from_discord.inc();
            debug!("[Discord] {}", &deunicode::deunicode(&msg.content));
            self.discord_msgs_tx
                .send(DiscordMessage::Chat {
                    nickname: msg
                        .author
                        .nick_in(ctx.http, guild)
                        .await
                        .unwrap_or(msg.clone().author.name),
                    msg,
                })
                .await
                .unwrap();
        }
    }
}

fn init_metrics() -> (Arc<TorvusMetrics>, Arc<Notify>) {
    let registry = Arc::new(Registry::new());
    let (torvus_metrics, registry_torvus_metrics) = TorvusMetrics::new().unwrap();
    let torvus_metrics_arc = Arc::new(torvus_metrics);
    registry_torvus_metrics(&registry).expect("failed to register torvus metrics");

    let metrics_server: SocketAddr = env::var("METRICS_SERVER")
        .expect("No environment variable 'METRICS_SERVER' found.")
        .parse()
        .expect("Invalid address!");

    let metrics_shutdown = Arc::new(Notify::new());
    let metrics_shutdown_clone = metrics_shutdown.clone();

    tokio::spawn(async move {
        PrometheusServer::run(
            Arc::clone(&registry),
            metrics_server,
            metrics_shutdown_clone.notified(),
        )
        .await
    });

    (torvus_metrics_arc, metrics_shutdown)
}

fn main() {
    let logger = tracing_subscriber::fmt::layer();
    let env_filter = EnvFilter::try_from_default_env()
        .unwrap()
        .add_directive("hyper=info".parse().unwrap())
        .add_directive("tower=info".parse().unwrap())
        .add_directive("tokio_util=info".parse().unwrap())
        .add_directive("h2=info".parse().unwrap())
        .add_directive(
            "serenity::client::bridge::gateway::shard_runner=info"
                .parse()
                .unwrap(),
        )
        .add_directive("mio=info".parse().unwrap());
    let collector = tracing_subscriber::Registry::default()
        .with(logger)
        .with(env_filter);

    // Initialize tracing
    tracing::subscriber::set_global_default(collector).unwrap();

    let runtime = Arc::new(
        tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .unwrap(),
    );
    let runtime2 = Arc::clone(&runtime);

    runtime.block_on(async {
        dotenv().ok();

        let token: String = env_key("DISCORD_TOKEN");
        let veloren_server = ConnectionArgs::Tcp {
            hostname: env::var("VELOREN_SERVER")
                .expect("No environment variable 'VELOREN_SERVER' found."),
            prefer_ipv6: false,
        };
        let guild = env_key("DISCORD_GUILD");
        let bridge_channel = env_key("DISCORD_CHANNEL");

        let veloren_username = env_key("VELOREN_USERNAME");
        let veloren_password = env_key("VELOREN_PASSWORD");
        let trusted_auth_server = env_key("VELOREN_TRUSTED_AUTH_SERVER");

        let (torvus_metrics, metrics_shutdown) = init_metrics();

        let handler = Handler::new(
            veloren_server,
            guild,
            bridge_channel,
            veloren_username,
            veloren_password,
            trusted_auth_server,
            torvus_metrics,
            metrics_shutdown,
            runtime2,
        );

        info!("Veloren-Common/Client version: {}", *DISPLAY_VERSION_LONG);

        let mut client = Client::builder(
            &token,
            GatewayIntents::GUILD_MESSAGES, /*.union(GatewayIntents::MESSAGE_CONTENT)*/
        )
        .event_handler(handler)
        .await
        .expect("Failed to create serenity client!");

        client.start().await.expect("Failed to start client.");
    });
}

fn env_key<T>(key: &str) -> T
where
    T: std::str::FromStr,
    <T as std::str::FromStr>::Err: std::fmt::Debug,
{
    env::var(key)
        .unwrap_or_else(|_| panic!("No environment variable '{}' found.", key))
        .parse()
        .unwrap_or_else(|_| panic!("'{}' couldn't be parsed.", key))
}
